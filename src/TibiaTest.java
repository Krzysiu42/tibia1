import java.util.Scanner;

class TibiaTest {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj swój poziom");
        int lvlnizej = scanner.nextInt();
        int lvl = lvlnizej + 1;

        // Formula is 50(x^2 - 5x + 8)

        double firstOperation = Math.pow(lvl,2);
        double secondOperation = 5 * lvl;
        double exp;

        exp = 50 * (firstOperation - secondOperation + 8);

        System.out.println(" Na poziomie " + lvlnizej + " potrzebujesz " + exp + " expa do następnego poziomu");


    }
}
